// Lint and build CSS
module.exports = function(grunt) {
  'use strict';

  grunt.registerTask('css', [
    'sass',
    'concat:cssDarkIX',
    'concat:cssDark',
    'concat:cssLight',
    'concat:cssFonts',
    'styleguide',
    'sasslint',
    'postcss'
    ]
  );

  grunt.registerTask('default', [
    'clean:gen',
    'copy:public_to_gen',
    'css',
    'exec:rsync'
  ])

};
