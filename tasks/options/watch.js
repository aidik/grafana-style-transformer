module.exports = function(config, grunt) {
  'use strict';

  grunt.event.on('watch', function(action, filepath) {
    var newPath;

    grunt.log.writeln('File Changed: ' + filepath);

    if (/(\.scss)$/.test(filepath)) {
      grunt.task.run('clean:css');
      grunt.task.run('css');
    }
      grunt.task.run('exec:rsync');
  });

  return {
    copy_to_gen: {
      files: ['<%= srcDir %>/**/*'],
      tasks: [],
      options: {
        spawn: false
      }
    },
  };
};
