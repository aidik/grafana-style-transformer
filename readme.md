# Grafana style transformer
Utility which changes style of Grafana to resemble iXsystems' FreeNAS 10.

## Requirements
To run this utility you need:

* node.js
* npm
* grunt
* rsync
* ssh *optional for acessing remote hosts*.

## Install 
```git clone https://bitbucket.org/aidik/grafana-style-transformer.git```

```cd grafana-style-transformer```

```npm install```

```cp config.example.yaml config.yaml``` *and edit it to your liking.*

## Usage
```service grafana-server stop``` *stop the instance of Grafana you want to transform*

```cd grafana-style-transformer```

```grunt```

```service grafana-server start```