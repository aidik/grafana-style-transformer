module.exports = function(config) {
  'use strict';

  return {
    rsync: {
  		cmd : "rsync -rvh <%= genDir %>/ <%= pathToHost %>"  
  	}
  };
};
